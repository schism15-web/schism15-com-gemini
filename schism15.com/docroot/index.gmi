          _     _
 ___  ___| |__ (_)___ _ __ ___
/ __|/ __| '_ \| / __| '_ ` _ \
\__ \ (__| | | | \__ \ | | | | |
|___/\___|_| |_|_|___/_| |_| |_|

Welcome to my Gemini capsule!

I'm not sure what I want this space to be, yet. For now I'm starting with keeping a dev log of projects I'm working on like the Gozer browser engine.

# Blog

=> devlog/20210220-1421-gozer-the-story-so-far.gmi  2020-02-20 Gozer: The Story So Far

# Projects

=> https://gitlab.com/schism15/gozer-engine     A browser enginer for the Gopher protocol
=> https://gitlab.com/schism15/gozer-curses     A command line client frontend build on the Gozer engine